import { combineReducers } from 'redux';
import LogReducer from './log/log.reducer';
import TechReducer from './tech/tech.reducer';

export default combineReducers({
    log: LogReducer,
    tech: TechReducer
})