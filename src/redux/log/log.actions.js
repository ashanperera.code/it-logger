import { GET_LOGS, SET_LOADING, LOGS_ERROR, ADD_LOG, DELETE_LOG, UPDATE_LOG, SET_CURRENT, SEARCH_LOGS } from '../actions/types';

//Get logs from the server.
export const getLogs = () => async dispatch => {
    try {
        setLoading()
        const serviceResponse = await fetch('/logs');
        const data = await serviceResponse.json();
        dispatch({
            type: GET_LOGS,
            payload: data
        });
    } catch (error) {
        dispatch({
            type: LOGS_ERROR,
            payload: error//.response.data
        })
    }
}

//Add logs
export const addLogs = (log) => async dispatch => {
    try {
        const serviceResponse = await fetch('/logs', {
            method: 'POST',
            body: JSON.stringify(log),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const data = await serviceResponse.json();
        if (data) {
            dispatch({
                type: ADD_LOG,
                payload: data
            })
        }
    } catch (error) {
        dispatch({
            type: LOGS_ERROR,
            payload: error.response.data
        })
    }
}

//Delete logs from server
export const deleteLog = (id) => async dispatch => {
    try {
        setLoading();
        await fetch(`/logs/${id}`, {
            method: 'DELETE'
        });
        dispatch({
            type: DELETE_LOG,
            payload: id
        })
    } catch (error) {
        dispatch({
            type: LOGS_ERROR,
            payload: error.response.data
        })
    }
}

//Get the current log
export const getCurrent = () => async dispatch => {
    try {

    } catch (error) {
        dispatch({
            type: LOGS_ERROR,
            payload: error.response.data
        })
    }
}

//Set current log.
export const setCurrent = (currentLog) => async dispatch => {
    try {
        setLoading();
        if (currentLog) {
            dispatch({
                type: SET_CURRENT,
                payload: currentLog
            })
        } else {
            dispatch({
                type: LOGS_ERROR,
                payload: 'Current log cannot be empty.'
            })
        }
    } catch (error) {
        dispatch({
            type: LOGS_ERROR,
            payload: error.response.data
        })
    }
}

//Update the log
export const updateLog = (log) => async dispatch => {
    try {
        if (log) {
            setLoading();
            const serviceResponse = await fetch(`/logs/${log.id}`, {
                method: 'PUT',
                body: JSON.stringify(log),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            const data = await serviceResponse.json();
            if (data) {
                dispatch({
                    type: UPDATE_LOG,
                    payload: data
                })
            }
        }

    } catch (error) {
        dispatch({
            type: LOGS_ERROR,
            payload: error.response.data
        })
    }
}

//Set loading
export const setLoading = () => {
    return {
        type: SET_LOADING
    }
}

//Search logs
export const searchLogs = (term) => dispatch => {
    dispatch({
        type: SEARCH_LOGS,
        payload: term
    })
}