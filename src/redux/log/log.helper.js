
const logSearchLogic = (state, term) => {
    if (term)
        return state.logs.filter(log => log.message.match(term) || log.tech.match(term));
    else
        return null;
}

export default logSearchLogic;