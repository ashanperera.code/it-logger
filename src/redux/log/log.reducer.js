import { GET_LOGS, SET_LOADING, LOGS_ERROR, ADD_LOG, DELETE_LOG, UPDATE_LOG, SET_CURRENT, SEARCH_LOGS } from '../actions/types';
import logSearchLogic from '../log/log.helper';

const initialState = {
    logs: null,
    searchedLogs: null,
    current: null,
    loading: false,
    error: null,
    isUpdated: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_LOGS:
            return {
                ...state,
                logs: action.payload,
                loading: false
            }
        case ADD_LOG:
            return {
                ...state,
                logs: [...state.logs, action.payload],
                loading: false
            }
        case DELETE_LOG:
            return {
                ...state,
                logs: state.logs.filter(log => log.id !== action.payload),
                loading: false
            }
        case SET_CURRENT:
            return {
                ...state,
                current: action.payload,
                loading: false,
                isUpdated: false
            }
        case UPDATE_LOG:
            return {
                ...state,
                logs: state.logs.map(log => log.id === action.payload.id ? action.payload : log),
                loading: false,
                isUpdated: true,
                current: null
            }
        case SET_LOADING:
            return {
                ...state,
                loading: true
            }
        case LOGS_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case SEARCH_LOGS:
            return {
                ...state,
                searchedLogs: logSearchLogic(state, action.payload),
                loading: false
            }
        default:
            return state;
    }
}