import { ADD_TECH, GET_TECHS, TECH_ERROR, DELETE_TECH, SET_LOADING } from '../actions/types';


//Get all techs
export const getAllTechs = () => async dispatch => {
    try {
        setLoading();
        const serviceResponse = await fetch('/techs', {
            method: 'GET'
        })
        const data = await serviceResponse.json();
        if (data) {
            dispatch({
                type: GET_TECHS,
                payload: data
            })
        }
    } catch (error) {
        dispatch({
            type: TECH_ERROR,
            payload: error.response.data
        })
    }
}

//Set the loading.
export const setLoading = () => dispatch => {
    dispatch({
        type: SET_LOADING
    })
}

//Delete the tech.
export const deleteTechs = (id) => async dispatch => {
    try {
        const serviceResponse = await fetch(`/techs/${id}`, {
            method: 'DELETE'
        })
        const data = await serviceResponse.json();
        if (data) {
            dispatch({
                type: DELETE_TECH,
                payload: id
            })
        }
    } catch (error) {
        dispatch({
            type: TECH_ERROR,
            payload: error.response.data
        })
    }
}

//Add the tech.
export const addTech = (tech) => async dispatch => {
    try {
        const serviceResponse = await fetch('/techs', {
            method: 'POST',
            body: JSON.stringify(tech),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const data = await serviceResponse.json();
        if (data) {
            dispatch({
                type: ADD_TECH,
                payload: data
            })
        }

    } catch (error) {
        dispatch({
            type: TECH_ERROR,
            payload: error.response.data
        })
    }
}