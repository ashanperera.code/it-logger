import { ADD_TECH, GET_TECHS, TECH_ERROR, DELETE_TECH, SET_LOADING } from '../actions/types';

const initialState = {
    techList: [],
    loading: false,
    error: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_TECHS:
            return {
                ...state,
                techList: action.payload,
                loading: false
            }
        case ADD_TECH:
            return {
                ...state,
                techList: [...state.techList, action.payload],
                loading: false
            }
        case DELETE_TECH:
            return {
                ...state,
                techList: state.techList.filter(x => x.id !== action.payload)
            }
        case TECH_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case SET_LOADING:
            return {
                ...state,
                loading: true
            }
        default:
            return state;
    }
}