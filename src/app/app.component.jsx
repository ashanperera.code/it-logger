import React, { useEffect, Fragment } from 'react'
import './app.component.scss';

import M from 'materialize-css/dist/js/materialize.min.js'
import 'materialize-css/dist/css/materialize.min.css';
//components
import SearchBar from './components/search-bar/search-bar.component';
import Logs from './components/logs/logs.component';
import AddButton from './components/button/add/add-button.component';
import AddLogModal from './components/modals/add/add-modal.component';
import EditModal from './components/modals/edit/edit-modal.component';
import AddTechModal from './components/techs/add-techs/add-tech.component';
import TechList from './components/techs/tech-list/tech-list.component';
//redux imports
import { Provider } from 'react-redux';
import store from '../redux/store';

const App = () => {

    useEffect(() => {
        //Init materialize.
        M.AutoInit()
    }, [])

    return (
        <Provider store={store}>
            <Fragment>
                <SearchBar />
                <div className="container">
                    <AddButton />
                    <AddLogModal />
                    <EditModal />
                    <Logs />
                    <AddTechModal />
                    <TechList />
                </div>
            </Fragment>
        </Provider>
    )
}

export default App;
