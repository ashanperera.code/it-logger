import React, { useEffect, useState } from 'react'
import TechItem from '../tech-item/tech-item.component';
import { getAllTechs } from '../../../../redux/tech/tech.actions';
import { connect } from 'react-redux';

const TechList = ({ tech, getAllTechs }) => {
    const { techList, loading } = tech;

    useEffect(() => {
        getAllTechs();
        // eslint-disable-next-line
    }, []);

    return (
        <div id="tech-list-modal" className="modal">
            <div className="modal-content">
                <h4>Technician List</h4>
                <ul className="collection">
                    {!loading && techList.map(tech => (
                        <TechItem tech={tech} key={tech.id} />
                    ))}
                </ul>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    tech: state.tech
})

export default connect(mapStateToProps, { getAllTechs })(TechList);