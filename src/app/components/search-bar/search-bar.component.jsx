import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { searchLogs } from '../../../redux/log/log.actions';

const SearchBar = ({log, searchLogs}) => {
    useEffect(() => {

    }, []);

    const onChangeEventHandler = (event) => {
        event.preventDefault();
        searchLogs(event.target.value);
    }

    return (
        <Fragment>
            <nav style={{ marginBottom: '30px' }} className='blue'>
                <div className="nav-wrapper">
                    <form>
                        <div className="input-field">
                            <input type="search" id="search" required onChange={onChangeEventHandler} />
                            <label htmlFor="search" className="label-icon"><i className="material-icons">search</i></label>
                            <i className="material-icons">close</i>
                        </div>
                    </form>
                </div>
            </nav>
        </Fragment>
    )
}

const mapStateToProps = state => ({
    log: state.log
})

export default connect(mapStateToProps, { searchLogs })(SearchBar);
