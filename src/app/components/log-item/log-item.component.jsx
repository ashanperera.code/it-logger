import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { deleteLog } from '../../../redux/log/log.actions';
import { setCurrent } from '../../../redux/log/log.actions';
import Moment from 'react-moment';
import M from 'materialize-css/dist/js/materialize.min.js'

const LogItem = ({ log, deleteLog, setCurrent }) => {

    useEffect(() => {
        M.AutoInit();
        // eslint-disable-next-line
    }, []);

    const onDeleteHandler = () => {
        deleteLog(log.id);
        M.toast({ html: 'Item deleted successfully.' })
    }

    const onEditHandler = () => {
        //dispatch to current
        setCurrent(log);
    }

    return (
        <Fragment>
            <li className="collection-item">
                <div>
                    <a href="#edit-log-modal" className={`modal-trigger ${log.attention ? 'red-text' : 'blue-text'}`} onClick={onEditHandler}>
                        {log.message}
                    </a>
                    <br />
                    <span className="grey-text">
                        <span className="black-text">ID # {log.id}</span>
                        <span className="black-text"> {log.tech} On </span>
                        <Moment format='MMMM Do YYYY h:mm:ss a'>{log.date}</Moment>
                    </span>
                    <a href="#!" className="secondary-content" onClick={onDeleteHandler}>
                        <i className="material-icons grey-text">delete</i>
                    </a>
                </div>
            </li>
        </Fragment>
    )
}

export default connect(null, { deleteLog, setCurrent })(LogItem);