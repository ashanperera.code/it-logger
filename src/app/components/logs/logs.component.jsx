import React, { useEffect } from 'react';
import LogItem from '../log-item/log-item.component';
import PreLoader from '../pre-loader/pre-loader.component';
import { connect } from 'react-redux';
import { getLogs } from '../../../redux/log/log.actions';

const Logs = ({ log: { logs, loading, searchedLogs }, getLogs }) => {

    useEffect(() => {
        if (!logs)
            getLogs();
        // eslint-disable-next-line
    }, [logs, searchedLogs])

    if (loading || logs === null) {
        return <PreLoader />
    }
    return (
        <ul className="collection with-header">
            <li className="collection-header">
                <h4 className="center">System Logs</h4>
            </li>
            {
                !loading && logs.length === 0 ?
                    (<p className="center">No logs to show...</p>) :
                    searchedLogs ?
                        (
                            searchedLogs.map(log => <LogItem key={log.id} log={log} />)
                        ) :
                        (
                            logs.map(log => <LogItem key={log.id} log={log} />)
                        )
            }
        </ul>
    )
}

const mapStateToProps = state => ({
    log: state.log
})

export default connect(mapStateToProps, { getLogs })(Logs);
